function DrawCell(i,j,alive)
    x_vertex = [i+0.5,i+0.5,i-0.5,i-0.5];
    y_vertex = [j+0.5,j-0.5,j-0.5,j+0.5];
    if alive == 1
        fill(x_vertex,y_vertex,'w');
    else
        fill(x_vertex,y_vertex,'k');
    end
end