function DrawBoard(board)
    rows = size(board,1);
    cols = size(board,2);
    
    hold on
    for i=1:rows
        for j=1:cols
            DrawCell(j,rows-i+1,board(i,j))
        end
    end
end

