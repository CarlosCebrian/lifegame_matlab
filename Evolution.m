function boards = Evolution(board,iters)
    boards = cell(1,iters);
    for i=1:iters
        boards{i} = Rules(board);
        board = boards{i};
        DibujaTablero(boards{i});
        pause(1);
        clf;
    end
end