function nextboard = Rules(board)
    board = Expansion(board);
    nextboard = board;
    rows = size(board,1);
    cols = size(board,2);
    
    for n=1:rows
        for m=1:cols
           cells_alive = CheckNeighbours(board,n,m);
           if board(n,m) == 1
               if cells_alive == 2 || cells_alive == 3
                   nextboard(n,m) = 1;
               else
                   nextboard(n,m) = 0;
               end
           else  
               if cells_alive == 3
                   nextboard(n,m) = 1;
               end
           end
        end
    end
    nextboard = Reduction(nextboard);
end

function cells_alive = CheckNeighbours(board,i,j)
    cells_alive = 0;
    for n=i-1:i+1
        if n >= 1 && n <= size(board,1)
            for m=j-1:j+1
                if m >= 1 && m <= size(board,2)
                    if ~(i == n && j == m)
                        if board(n,m) == 1
                            cells_alive = cells_alive + 1;
                        end
                    end
                end
            end
        end
    end
end

function expansion_board = Expansion(board)
    rows = size(board,1);
    cols = size(board,2);
    expansion_board = zeros(rows + 2,cols + 2);
    expansion_board(2:rows+1,2:cols+1) = board;
end

function reduction_board = Reduction(board)
    keep_cols = find(sum(board,1));
    keep_rows = find(sum(board,2));
    reduction_board = board((keep_rows),(keep_cols));
end